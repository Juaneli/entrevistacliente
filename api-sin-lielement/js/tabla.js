fetch('https://api.datos.gob.mx/v1/calidadAire').then(response => response.json()).then((Datos) => {
    let resultados = Datos.results 
    let body = ''
    console.log(resultados)
    document.getElementById('data').innerHTML = body
    resultados.forEach( r => {
        r.stations.forEach(station => {
            body += `<tr>${Body(station)}</tr>`
        })
    })
    document.getElementById('data').innerHTML = body
})

function getIndexex(indexes) {
    let data = ''
    indexes.forEach(indexes => {
        data += `<td>${indexes.calculationTime}</td><td>${indexes.responsiblePollutant}</td><td>${indexes.value}</td><td>${indexes.scale}</td>`
    })
    return data
}

function getMeasurements (measurements){
    let data= ''
    measurements.forEach(ms => {
        data += `<td>${ms.averagedOverInHours}</td><td>${ms.time}</td><td>${ms.value}</td><td>${ms.unit}</td><td>${ms.pollutant}</td>`
    })
    return data
}

function Body(station) {
    var arrayIndexes = getIndexex(station.indexes)
    var arrayMeasurements = getMeasurements(station.measurements)
    return `<td>${station.name}</td><td>${station.id}</td> ${arrayIndexes} ${arrayMeasurements}`
}