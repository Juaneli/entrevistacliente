import { html, css, LitElement } from 'lit';
import './components/GetData'

export class CalidadAire extends LitElement {
  static get styles() {
    return css`
      
    `;
  }

  static get properties() {
    return {
      wiki: {type: Array},
      wiki2: {type: Array},
      wiki3: {type: Array},
    };
  }
  constructor() {
    super();

    this.wiki = []
    this.wiki2 = []
    this.wiki3 = []

    this.addEventListener('ApiData', (e) => {
      console.log(e.detail.data.results)
      this._dataFormat(e.detail.data.results)
    })
  }

  _dataFormat(data){
    const datos = []
    const stations = [];
    const indexes = [];
    const measurements = [];
    data.forEach(s => {
      s.stations.forEach(st => {
        stations.push({
          id: st.id,
          name: st.name
        })
      })
    });

    data.forEach(s => {
      s.stations.forEach(st => {
        st.indexes.forEach(i => {
          indexes.push({
            calculationTime: i.calculationTime,
            responsiblePollumant: i.responsiblePollumant,
            value: i.value,
            scale: i.scale
          })
        })
      })
    })

    data.forEach(s => {
      s.stations.forEach(st => {
        st.measurements.forEach( m => {
          measurements.push({
            averagedOverInHours: m.averagedOverInHours,
            time: m.time,
            value: m.value,
            unit: m.unit,
            pollutant: m.pollutant
          })
        })
      })
    })


    this.wiki = stations
    this.wiki2 = indexes
    this.wiki3 = measurements
  }

  render() {
    return html`
    <get-data url="https://api.datos.gob.mx/v1/calidadAire" method="GET"></get-data>
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Id</th>
          <th>CalculationTime</th>
          <th>responsiblePollutant</th>
          <th>Value</th>
          <th>Scale</th>
          <th>AveragedOverInHours</th>
          <th>Time</th>
          <th>Value</th>
          <th>unit</th>
          <th>Pollutant</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        ${this.dateTemplate}
        </tr>
      </tbody>
    </table>
    `;
  }

  get dateTemplate() {
    return html `  
    ${this.wiki.map(datos => html `
    <tr></tr>
    <td>${datos.name}</td>
    <td>${datos.id}</td>
    `)}
    ${this.wiki2.map(datos => html `
    <tr></tr>
    <td>${datos.calculationTime}</td>
    <td>${datos.responsiblePollumant}</td>
    <td>${datos.value}</td>
    <td>${datos.scale}</td>
    `)}
    ${this.wiki3.map(datos => html `
    <tr></tr>
    <td>${datos.averagedOverInHours}</td>
    <td>${datos.time}</td>
    <td>${datos.value}</td>
    <td>${datos.unit}</td>
    <td>${datos.pollutant}</td>
    `)}
    `;
  }
}

